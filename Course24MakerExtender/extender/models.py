from django.db import models


class Category(models.Model):

    remote_id = models.CharField(max_length=100, null=False, unique=True)
    label = models.CharField(max_length=100, null=False)
    price = models.FloatField(null=True)
    va_price = models.FloatField(null=True)


class Team(models.Model):

    remote_id = models.CharField(max_length=100, null=False, unique=True)
    name = models.CharField(max_length=50, null=False)
    category = models.ForeignKey(
        Category, related_name='teams', on_delete=models.CASCADE)


class Athlete(models.Model):

    remote_id = models.CharField(max_length=100, null=False, unique=True)
    first_name = models.CharField(max_length=50, null=False)
    last_name = models.CharField(max_length=50, null=False)
    birthdate = models.DateField(null=False)
    phone = models.CharField(max_length=20, null=False)
    email = models.EmailField(null=False)
    team = models.ForeignKey(
        Team, null=True, related_name='athletes', on_delete=models.CASCADE)

    class Meta:
        ordering = ["id"]
        verbose_name = "Athlete"
        verbose_name_plural = "Athletes"


class Payment(models.Model):

    command_reference = models.BigIntegerField(null=False, unique=True)
    bank_reference = models.BigIntegerField(null=False, blank=True)
    date = models.DateField(null=False, blank=True)
    paid = models.FloatField(null=False)
    first_name = models.CharField(max_length=50, null=False)
    last_name = models.CharField(max_length=50, null=False)
    email = models.EmailField(null=False)
    category = models.CharField(max_length=50, null=True)
    va = models.BooleanField(default=False)


class AthletePayment(models.Model):
    athlete = models.OneToOneField(
        Athlete, null=False, related_name="payment", on_delete=models.CASCADE)
    payment = models.OneToOneField(
        Payment, null=False, related_name="athlete", on_delete=models.CASCADE)
    status = models.IntegerField(default=0)


class Certificate(models.Model):
    filename = models.CharField(max_length=100, null=True)
    status = models.IntegerField(default=0)
    athlete = models.OneToOneField(
        Athlete, unique=True, related_name='certificate', on_delete=models.CASCADE)


class VA(models.Model):
    number = models.CharField(max_length=20, null=True)
    filename = models.CharField(max_length=100, null=True)
    status = models.IntegerField(default=0)
    athlete = models.OneToOneField(
        Athlete, null=True, unique=True, related_name='va', on_delete=models.CASCADE)


class ReloadEvent(models.Model):
    date = models.DateTimeField(auto_now=True)
