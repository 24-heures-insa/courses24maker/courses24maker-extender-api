from rest_framework import serializers
from .models import Category, Team, Athlete, Certificate, VA, Payment, AthletePayment


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = "__all__"


class CategoryLightSerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = "__all__"


class TeamSerializer(serializers.ModelSerializer):

    class Meta:
        model = Team
        fields = "__all__"


class TeamLightSerializer(serializers.ModelSerializer):
    category = CategoryLightSerializer()

    class Meta:
        model = Team
        fields = ['id', 'name', 'category']
        read_only_fields = ['id', 'name']


class CertificateLightSerializer(serializers.ModelSerializer):

    class Meta:
        model = Certificate
        fields = ['id', 'status']


class VALightSerializer(serializers.ModelSerializer):

    class Meta:
        model = VA
        fields = ['id', 'status']


class PaymentLightSerializer(serializers.ModelSerializer):

    class Meta:
        model = Payment
        fields = ['id', 'command_reference', 'paid', 'va', 'category']


class AthleteSerializer(serializers.ModelSerializer):
    team = TeamLightSerializer()
    certificate = CertificateLightSerializer()
    va = VALightSerializer()

    class Meta:
        model = Athlete
        fields = ['id', 'first_name', 'last_name', 'birthdate',
                  'email', 'phone', 'team', 'certificate', 'va']
        read_only_fields = ['id', 'first_name', 'last_name']


class AthleteLightSerializer(serializers.ModelSerializer):
    team = TeamLightSerializer()
    certificate = CertificateLightSerializer()
    va = VALightSerializer()

    class Meta:
        model = Athlete
        fields = ['id', 'first_name', 'last_name', 'team', 'certificate', 'va']
        read_only_fields = ['id', 'first_name', 'last_name']


class CertificateSerializer(serializers.ModelSerializer):
    athlete = AthleteLightSerializer()

    class Meta:
        model = Certificate
        fields = ['id', 'status', 'athlete']


class VASerializer(serializers.ModelSerializer):
    athlete = AthleteLightSerializer()

    class Meta:
        model = VA
        fields = ['id', 'status', 'athlete']

class AthletePaymentLightSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = AthletePayment
        fields = ['id', 'status']


class PaymentSerializer(serializers.ModelSerializer):
    athlete = AthletePaymentLightSerializer()

    class Meta:
        model = Payment
        fields = ['id', 'command_reference', 'paid', 'first_name', 'date',
                  'last_name', 'email', 'category', 'va', "athlete"]


class AthletePaymentSerializer(serializers.ModelSerializer):
    athlete = AthleteLightSerializer()
    payment = PaymentLightSerializer()

    class Meta:
        model = AthletePayment
        fields = ['id', 'status', 'athlete', 'payment']
