from django.apps import AppConfig


class ExtenderConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'extender'
