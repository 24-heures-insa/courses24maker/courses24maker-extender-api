from django.urls import include, path, re_path
from rest_framework import routers
from django.contrib.auth import views as auth_views
from extender import views

router = routers.DefaultRouter()
router.register(r'athletes', views.AthleteViewSet, basename='Athletes')
router.register(r'categories', views.CategoryViewSet, basename='Categories')
router.register(r'certificates', views.CertificateViewSet,
                basename='Certificates')
router.register(r'payments', views.PaymentViewSet, basename='Payments')
router.register(r'vas', views.VAViewSet, basename='VA')
router.register(r'athlete_payments', views.AthletePaymentViewSet,
                basename='AthletePayment')

urlpatterns = [
    path('', include(router.urls)),
    path('token/', views.access_token, name="token"),
    path('token/refresh/', views.refresh_tocken, name="refresh token"),
    re_path(r'^upload/(?P<filename>[^/]+)$',
            views.PaymentsFileUploadView.as_view()),
    path('reload/', views.reload)
]
