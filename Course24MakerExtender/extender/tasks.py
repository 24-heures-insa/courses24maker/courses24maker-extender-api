import os
import requests
from celery import shared_task
from .models import Category, Team, Athlete, VA, Certificate, Payment
from django.db import models

base_url = os.getenv('BASE_URL')
headers = {"Authorization": f"Bearer {os.getenv('TOKEN')}"}


@shared_task
def load_categories():
    print("Loading Categories...")
    response = requests.get(
        url=base_url + "/categories",
        headers=headers
    )
    response.raise_for_status()
    if response.status_code == 200:
        categories = response.json()['categories']
        n = 0
        nb_categories = len(categories)
        print(f"{nb_categories} categories found.")
        for category in categories:
            n += 1
            new_category, created = Category.objects.get_or_create(
                remote_id=category['category_id']
            )
            new_category.label = category['category_label']
            new_category.price = category['category_price_regular']
            new_category.va_price = category['category_price_student']
            new_category.save()
            print(
                f"[{n}/{nb_categories}] Category '{new_category.label}' loaded...")


@shared_task
def load_teams():
    print("Loading Teams...")
    response = requests.get(
        url=base_url + "/teams",
        headers=headers
    )
    response.raise_for_status()
    if response.status_code == 200:
        teams = response.json()['teams']
        n = 0
        nb_teams = len(teams)
        print(f"{nb_teams} teams found.")
        for team in teams:
            n += 1
            category = Category.objects.get(
                remote_id=team['team_category']['category_id']
            )
            try:
                new_team = Team.objects.get(
                    remote_id=team['team_id']
                )
                new_team.category=category
                new_team.name = team['team_name']
                new_team.save()
            except models.ObjectDoesNotExist:
                new_team, created = Team.objects.get_or_create(
                    remote_id=team['team_id'],
                    category=category
                )
            print(
                f"[{n}/{nb_teams}] Team '{new_team.name}' loaded...")


@shared_task
def load_athletes():
    print("Loading Atheltes...")
    response = requests.get(
        url=base_url + "/participants",
        headers=headers
    )
    response.raise_for_status()
    if response.status_code == 200:
        athletes = response.json()['participants']
        n = 0
        nb_athletes = len(athletes)
        print(f"{nb_athletes} athletes found.")
        athlete_ids = []
        for athlete in athletes:
            n += 1
            team = Team.objects.get(
                remote_id=athlete["participant_team_id"]
            )
            try:
                new_athlete = Athlete.objects.get(
                    remote_id=athlete['participant_id'],
                )
                new_athlete.first_name=athlete['participant_name']
                new_athlete.last_name=athlete['participant_surname']
                new_athlete.birthdate=athlete['participant_birthdate']
                new_athlete.phone=athlete['participant_telephone']
                new_athlete.email=athlete['participant_email']
                new_athlete.team=team
                new_athlete.save()
                athlete_ids.append(new_athlete.remote_id)
            except models.ObjectDoesNotExist:
                new_athlete, created = Athlete.objects.get_or_create(
                    remote_id=athlete['participant_id'],
                    first_name=athlete['participant_name'],
                    last_name=athlete['participant_surname'],
                    birthdate=athlete['participant_birthdate'],
                    phone=athlete['participant_telephone'],
                    email=athlete['participant_email'],
                    team=team
                )
                athlete_ids.append(new_athlete.remote_id)
            if athlete['participant_student'] == 1:
                va, created = VA.objects.get_or_create(
                    athlete=new_athlete
                )
                if athlete['participant_student_certificate_file']:
                    va.filename = athlete['participant_student_certificate_file']
                else:
                    va_files = [f for f in os.listdir(
                        "./static/vas/") if os.path.isfile(os.path.join("./static/vas/", f))]
                    for va_file in va_files:
                        if va_file.startswith(f"student_certificate_{athlete['participant_id']}"):
                            va.filename = va_file                 
                va.status = athlete['participant_student_certificate_valid']
                va.save()
            certificate, created = Certificate.objects.get_or_create(
                athlete=new_athlete
            )
            if athlete['participant_medical_certificate_file']:
                certificate.filename = athlete['participant_medical_certificate_file']
            else:
                certificate_files = [f for f in os.listdir(
                    "./static/certificates/") if os.path.isfile(os.path.join("./static/certificates/", f))]
                for certificate_file in certificate_files:
                    if certificate_file.startswith(f"medical_certificate_{athlete['participant_id']}"):
                        certificate.filename =  certificate_file     
            certificate.status = athlete['participant_medical_certificate_valid']
            certificate.save()
            new_athlete.save()
            print(
                f"[{n}/{nb_athletes}] Athlete '{new_athlete.first_name} {new_athlete.last_name}' loaded...")
        athletes_data = Athlete.objects.all()
        for athlete in athletes_data:
            if athlete.remote_id not in athlete_ids:
                athlete.delete()

@shared_task
def load_certificates():
    print("Loading Certificates...")


@shared_task
def load_vas():
    print("Loading VAs...")


if __name__ == "__main__":
    load_athletes()
