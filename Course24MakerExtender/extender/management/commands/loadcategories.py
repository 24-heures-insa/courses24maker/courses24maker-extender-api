from django.core.management.base import BaseCommand, CommandError
from extender.tasks import load_categories

class Command(BaseCommand):
    help = 'Load all categories created in the remote API'
    
    def handle(self, *args, **options):
        load_categories()
        self.stdout.write(self.style.SUCCESS('Successfully loaded categories'))