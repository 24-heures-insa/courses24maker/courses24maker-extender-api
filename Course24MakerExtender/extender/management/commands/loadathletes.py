from django.core.management.base import BaseCommand, CommandError
from extender.tasks import load_athletes


class Command(BaseCommand):
    help = 'Load all athletes created in the remote API'

    def handle(self, *args, **options):
        load_athletes()
        self.stdout.write(self.style.SUCCESS('Successfully loaded athletes'))
