from django.core.management.base import BaseCommand, CommandError
from extender.tasks import load_teams


class Command(BaseCommand):
    help = 'Load all teams created in the remote API'

    def handle(self, *args, **options):
        load_teams()
        self.stdout.write(self.style.SUCCESS('Successfully loaded teams'))
