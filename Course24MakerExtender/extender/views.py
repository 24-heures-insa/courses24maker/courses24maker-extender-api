import os
import io
import requests
import json
import urllib
import base64

from requests.exceptions import HTTPError
from django.http import FileResponse
from django.db import models, IntegrityError
from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, mixins, status, views, filters
from rest_framework.response import Response
from rest_framework.decorators import action, permission_classes, api_view
from rest_framework.permissions import AllowAny
from rest_framework.parsers import FileUploadParser
from openpyxl import load_workbook
from pdf2image import convert_from_path

from .tasks import load_categories, load_teams, load_athletes
from .models import Athlete, Certificate, VA, Payment, Category, AthletePayment
from .serializers import AthleteSerializer, AthleteLightSerializer, CertificateSerializer, VASerializer, PaymentSerializer, PaymentLightSerializer, CategoryLightSerializer, AthletePaymentSerializer

base_url = os.getenv('BASE_URL')
headers = {'Authorization': f"Bearer {os.getenv('TOKEN')}"}


def get_athlete_data(athlete_remote_id: int) -> dict:
    response = requests.get(
        url=base_url + "/participants/" + athlete_remote_id,
        headers=headers
    )
    response.raise_for_status()
    if response.status_code == 200:
        data = response.json()['participant']
        data.pop('updatedAt', None)
        data.pop('createdAt', None)
        data.pop('participant_message', None)
        data.pop('participant_comment', None)
        print(type(data))
        return data


def put_athlete_data(athlete_remote_id: str, data: dict) -> bool:
    response = requests.put(
        url=base_url + "/participants/" + str(athlete_remote_id),
        data=json.dumps({
            "participant": data
        }),
        headers={
            'Authorization': f"Bearer {os.getenv('TOKEN')}",
            "Content-Type": "application/json"
        }
    )
    print(response.status_code)
    print(response)
    if response.status_code == 204:
        return True


class AthleteViewSet(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):

    queryset = Athlete.objects.all()
    serializer_class = AthleteLightSerializer
    filter_backends = (filters.SearchFilter, DjangoFilterBackend, )
    search_fields = ['first_name', 'last_name', 'team__name']
    filter_fields = ['team__category__id',
                     'va__status', 'certificate__status', ]
    tags = ["Athletes"]

    def retrieve(self, request, *args, **kwargs):
        print(args)
        try:
            athlete = Athlete.objects.get(id=self.kwargs['pk'])
        except models.ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND, data={'err': f"Athlete {args['pk']} not found."})
        return Response(data=AthleteSerializer(athlete).data)


class CategoryViewSet(
    mixins.ListModelMixin,
    viewsets.GenericViewSet
):

    queryset = Category.objects.all()
    serializer_class = CategoryLightSerializer


class CertificateViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):

    queryset = Certificate.objects.all()
    serializer_class = CertificateSerializer

    def retrieve(self, request, *args, **kwargs):
        return super(CertificateViewSet, self).retrieve(request=request, args=args, kwargs=kwargs)

    @action(detail=True, methods=['GET'])
    def picture(self, request, pk=None):
        try:
            certificate = Certificate.objects.get(id=pk)
            if certificate.filename is not None:
                if certificate.filename.endswith("pdf"):
                    pages = convert_from_path(
                        f"./static/certificates/{certificate.filename}")
                    pages[0].save(
                        f"./static/certificates/{certificate.filename[:-3]}jpeg", 'JPEG')
                    certificate.filename = f"{certificate.filename[:-3]}jpeg"
                    certificate.save()
                return FileResponse(open(f"./static/certificates/{certificate.filename}", 'rb'))
        except models.ObjectDoesNotExist as err:
            print(err)
            return Response(status=status.HTTP_404_NOT_FOUND, data={'err': f'Certificate {pk} not found.'})

        return Response(data=CertificateSerializer(certificate).data)

    @action(detail=True, methods=['POST'])
    def validate(self, request, pk=None):
        try:
            certificate = Certificate.objects.get(id=pk)
        except models.ObjectDoesNotExist as err:
            print(err)
            return Response(status=status.HTTP_404_NOT_FOUND, data={'err': f'Certificate {pk} not found.'})
        try:
            certificate = Certificate.objects.get(id=pk)
        except models.ObjectDoesNotExist as err:
            print(err)
            return Response(status=status.HTTP_404_NOT_FOUND, data={'err': f'Certificate {pk} not found.'})
        try:
            data = get_athlete_data(certificate.athlete.remote_id)
            data["participant_medical_certificate_valid"] = 1
            put_athlete_data(certificate.athlete.remote_id, data)
            certificate.status = 1
            certificate.save()
        except HTTPError as err:
            print(err)
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data={'err': 'Internal error.'})
        return Response(data=CertificateSerializer(certificate).data)

    @action(detail=True, methods=['POST'])
    def reject(self, request, pk=None):
        try:
            certificate = Certificate.objects.get(id=pk)
        except models.ObjectDoesNotExist as err:
            print(err)
            return Response(status=status.HTTP_404_NOT_FOUND, data={'err': f'Certificate {pk} not found.'})
        try:
            data = get_athlete_data(certificate.athlete.remote_id)
            data["participant_medical_certificate_valid"] = 2
            put_athlete_data(certificate.athlete.remote_id, data)
            certificate.status = 2
            certificate.save()
        except HTTPError as err:
            print(err)
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data={'err': 'Internal error.'})
        return Response(data=CertificateSerializer(certificate).data)


class VAViewSet(
        mixins.ListModelMixin,
        mixins.RetrieveModelMixin,
        viewsets.GenericViewSet):

    queryset = VA.objects.all()
    serializer_class = VASerializer

    def retrieve(self, request, *args, **kwargs):
        return super(VAViewSet, self).retrieve(request=request, args=args, kwargs=kwargs)

    @action(detail=True, methods=['GET'])
    def picture(self, request, pk=None):
        try:
            va = VA.objects.get(id=pk)
            if va.filename is not None:
                if va.filename.endswith("pdf"):
                    pages = convert_from_path(
                        f"./static/vas/{va.filename}")
                    pages[0].save(
                        f"./static/vas/{va.filename[:-3]}jpeg", 'JPEG')
                    va.filename = f"{va.filename[:-3]}jpeg"
                    va.save()
                return FileResponse(open(f"./static/vas/{va.filename}", 'rb'))
        except models.ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND, data={'err': f"VA {pk} not found."})

    @action(detail=True, methods=['POST'])
    def check(self, request, pk=None):
        first_name = request.data.get('first_name')
        last_name = request.data.get('last_name')
        card = request.data.get('card')

        if(first_name and last_name and card):
            try:
                va = VA.objects.get(id=pk)
            except models.ObjectDoesNotExist as err:
                print(err)
                return Response(status=status.HTTP_404_NOT_FOUND, data={'err': f"VA {pk} not found."})
            try:
                other_va = VA.objects.get(number=card)
                if other_va.id == va.id:
                    data = get_athlete_data(va.athlete.remote_id)
                    data["participant_student_certificate_valid"] = 1
                    put_athlete_data(va.athlete.remote_id, data)
                    return Response(status=status.HTTP_200_OK, data={'info': 'VA already registered.'})
                else:
                    data = get_athlete_data(va.athlete.remote_id)
                    data["participant_student_certificate_valid"] = 2
                    put_athlete_data(va.athlete.remote_id, data)
                    return Response(status=status.HTTP_400_BAD_REQUEST, data={'err': "VA already registered."})
            except models.ObjectDoesNotExist as err:
                print(err)
                pass
            try:
                response = requests.post(
                    url=os.getenv("EDB_SSO_URL"),
                    data={
                        'client_id': os.getenv("EDB_CLIENT_ID"),
                        'client_secret': os.getenv("EDB_CLIENT_SECRET"),
                        'grant_type': 'client_credentials'
                    }

                )
                response.raise_for_status()
                if response.status_code == 200:
                    access_token = response.json()['access_token']
                    print(access_token)
                    print(os.getenv('EDB_VA_API_URL'))
                    print(json.dumps({
                        "last_name": last_name,
                        "first_name": first_name,
                        "card": card
                    }))
                    response = requests.post(
                        url=os.getenv('EDB_VA_API_URL'),
                        data=json.dumps({
                            "last_name": last_name,
                            "first_name": first_name,
                            "card": card
                        }),
                        headers={
                            'Authorization': f"Bearer {access_token}",
                            "Content-type": "application/json"
                        }
                    )
                    print(response.json())

                    if response.status_code < 300:
                        try:
                            va.number = card
                            va.save()
                        except IntegrityError as err:
                            print(err)
                            return Response(status=status.HTTP_400_BAD_REQUEST, data={'err': 'VA card already used'})
                        try:
                            data = get_athlete_data(va.athlete.remote_id)
                            data["participant_student_certificate_valid"] = 1
                            put_athlete_data(va.athlete.remote_id, data)
                            va.status = 1
                            va.number = card
                            va.save()
                        except HTTPError as err:
                            print(err)
                            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data={'err': 'Internal error.'})
                        return Response(status=status.HTTP_200_OK, data={'info': 'VA card validated'})
                    else:
                        try:
                            data = get_athlete_data(va.athlete.remote_id)
                            data["participant_student_certificate_valid"] = 2
                            put_athlete_data(va.athlete.remote_id, data)
                            va.status = 2
                            va.number = card
                            va.save()
                        except HTTPError as err:
                            print(err)
                            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data={'err': 'Internal error.'})
                        return Response(status=status.HTTP_400_BAD_REQUEST, data={'err': 'No va found from EDB database'})
            except HTTPError as err:
                print(err)
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data={'err': 'Internal error'})
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'err': "Empty value in body"})

    @action(detail=True, methods=['POST'])
    def validate(self, request, pk=None):
        try:
            va = VA.objects.get(id=pk)
        except models.ObjectDoesNotExist as err:
            print(err)
            return Response(status=status.HTTP_404_NOT_FOUND, data={'err': f"VA {pk} not found."})
        try:
            data = get_athlete_data(va.athlete.remote_id)
            data["participant_student_certificate_valid"] = 1
            put_athlete_data(va.athlete.remote_id, data)
            va.status = 1
            va.save()
        except HTTPError as err:
            print(err)
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data={'err': 'Internal error.'})
        return Response(data=VASerializer(va).data)

    @action(detail=True, methods=['POST'])
    def reject(self, request, pk=None):
        try:
            va = VA.objects.get(id=pk)
        except models.ObjectDoesNotExist as err:
            print(err)
            return Response(status=status.HTTP_404_NOT_FOUND, data={'err': f"VA {pk} not found."})
        try:
            data = get_athlete_data(va.athlete.remote_id)
            data["participant_student_certificate_valid"] = 2
            put_athlete_data(va.athlete.remote_id, data)
            va.status = 2
            va.save()
        except HTTPError as err:
            print(err)
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data={'err': 'Internal error.'})
        return Response(data=VASerializer(va).data)


class PaymentsFileUploadView(views.APIView):

    parser_classes = [FileUploadParser]
    filter_backends = (filters.SearchFilter, DjangoFilterBackend, )
    search_fields = ['first_name', 'last_name', 'category']
    filter_fields = ['va', 'athletepayment__status']

    def put(self, request, filename, format=None):
        file = request.data['file']
        wb = load_workbook(filename=io.BytesIO(file.read()))
        ws = wb.worksheets[0]
        for i in range(2, ws.max_row+1):
            payment = {}
            for j in range(1, ws.max_column+1):
                cell_obj = ws.cell(row=i, column=j)
                payment[j] = str(cell_obj.value).replace(
                    "\"", "").replace("=", "")
            date = f'{payment[5].split(" ")[0][-4:]}-{payment[5].split(" ")[0][3:5]}-{payment[5].split(" ")[0][0:2]}'
            try:
                updated_payment = Payment.objects.get(
                    command_reference=payment[1])
                updated_payment.paid = payment[11]
                updated_payment.save()
            except models.ObjectDoesNotExist:
                new_payment, created = Payment.objects.get_or_create(
                    command_reference=payment[1],
                    bank_reference=payment[2],
                    date=date,
                    paid=payment[6],
                    first_name=payment[15],
                    last_name=payment[16],
                    email=payment[17],
                    category=payment[22]
                )
        return Response(status=204)


class PaymentViewSet (mixins.ListModelMixin,
                      mixins.RetrieveModelMixin,
                      mixins.CreateModelMixin,
                      viewsets.GenericViewSet):

    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer

    def retrieve(self, request, *args, **kwargs):
        try:
            payment = Payment.objects.get(id=self.kwargs['pk'])
        except models.ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND, data={'err': f"Payment {args['pk']} not found."})
        return Response(data=PaymentSerializer(payment).data)

    @action(detail=True, methods=['POST'])
    def associate_athlete(self, request):
        print("Hello")


class AthletePaymentViewSet(mixins.ListModelMixin,
                            mixins.RetrieveModelMixin,
                            mixins.CreateModelMixin,
                            viewsets.GenericViewSet):
    queryset = AthletePayment.objects.all()
    serializer_class = AthletePaymentSerializer

    def create(self, request, *args, **kwargs):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        athlete_id = body.get("athlete_id")
        payment_id = body.get("payment_id")
        try:
            athlete = Athlete.objects.get(id=athlete_id)
        except models.ObjectDoesNotExist as error:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'err': 'Athlete not found.'})
        try:
            payment = Payment.objects.get(id=payment_id)
        except models.ObjectDoesNotExist as error:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'err': 'Payment not found.'})
        try:
            athlete_payment = AthletePayment.objects.create(
                payment=payment,
                athlete=athlete,
                status=0
            )
            return Response(data=AthletePaymentSerializer(athlete_payment).data)
        except IntegrityError as error:
            print(error)
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'err': 'Association already exists.'})

    @action(detail=True, methods=['POST'])
    def validate(self, request, pk=None):
        try:
            payment = AthletePayment.objects.get(id=pk)
        except models.ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND, data={'err': f'Payment {pk} not found.'})
        try:
            data = get_athlete_data(payment.athlete.remote_id)
            print(data)
            data["participant_payment"] = 1
            data["participant_tee_shirt_size"] = "NO"
            put_athlete_data(payment.athlete.remote_id, data)
            payment.status = 1
            payment.save()
        except HTTPError:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data={'err': 'Internal error.'})
        return Response(data=AthletePaymentSerializer(payment).data)

    @action(detail=True, methods=['POST'])
    def reject(self, request, pk=None):
        try:
            payment = AthletePayment.objects.get(id=pk)
        except models.ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND, data={'err': f'Payment {pk} not found.'})
        try:
            data = get_athlete_data(payment.athlete.remote_id)
            data["participant_payment"] = 2
            data["participant_tee_shirt_size"] = "NO"
            put_athlete_data(payment.athlete.remote_id, data)
            payment.status = 2
            payment.save()
        except HTTPError:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data={'err': 'Internal error.'})
        return Response(data=AthletePaymentSerializer(payment).data)


@api_view(['POST'])
@permission_classes([AllowAny])
def access_token(request):
    username = request.POST.get("username")
    password = request.POST.get("password")
    if request.method == 'POST':
        user = authenticate(username=username, password=password)
        if user:
            if Athlete.objects.filter(user=user).exists():
                athlete = Athlete.objects.get(user=user)
                refresh = RefreshToken.for_user(user)
                return Response(
                    {
                        "id": athlete.id,
                        "access": str(refresh.access_token),
                        "refresh": str(refresh)
                    }
                )
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([AllowAny])
def refresh_tocken(request):
    refresh = request.POST.get("refresh")
    if refresh:
        refresh_token = RefreshToken(refresh)
        return Response(
            {
                "access": str(refresh_token.access_token),
                "refresh": str(refresh_token)
            }
        )
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def reload(request):
    load_categories()
    load_teams()
    load_athletes()
