FROM python:3.8-slim

WORKDIR /app

ENV VERSION = 1
ENV SECRET_KEY = ""
ENV ALLOWED_HOSTS = "localhost 127.0.0.1 0.0.0.0"
ENV DEBUG = 0
ENV SQL_ENGINE = "django.db.backends.sqlite3"
ENV SQL_DATABASE = "db.sqlite3"
ENV SQL_USER = "user"
ENV SQL_PASSWORD = "password"
ENV SQL_HOST = "localhost"
ENV SQL_PORT = "5432"


COPY Pipfile . 
COPY Pipfile.lock . 
COPY Course24MakerExtender ./Course24MakerExtender

RUN apt-get update &&  apt-get install -y python3-dev default-libmysqlclient-dev build-essential poppler-utils

RUN pip install --no-cache-dir --upgrade pipenv==2021.11.23 && \
    pipenv install --system --deploy --ignore-pipfile && \       
    pipenv --clear && \     
    rm Pipfile Pipfile.lock 

WORKDIR /app/Course24MakerExtender 

EXPOSE 8000

CMD python manage.py collectstatic --no-input &&\
    python manage.py makemigrations &&\
    python manage.py migrate &&\    
    python manage.py migrate django_celery_results && \
    gunicorn Course24MakerExtender.wsgi --bind 0.0.0.0:8000